use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum Size {
    Small,
    Medium,
    Large,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Color {
    Red,
    Yellow,
    Blue,
    Orange,
    Green,
    Purple,
    White,
    Black,
    Gray,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Material {
    Steel,
    Aluminum,
    Plastic,
    Concrete,
    RedOak,
    WhiteMaple,
    WhiteOak,
    WhitePine,
    Styrofoam,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Bar {
    pub size: Size,
    pub color: Color,
    pub material: Material,
}
